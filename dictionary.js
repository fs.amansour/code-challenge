const fs = require('fs');
const fileName = 'data.json';
let fileData;

class Dictionary {
    async createFileIfNotExisted() {
        if (!fs.existsSync(fileName)) {
            await fs.appendFileSync(fileName, '{}');
            return 'File created';
        }
    };

    async readFile() {
        await this.createFileIfNotExisted();
        console.log('Reading file ...');
        const data = await fs.readFileSync(fileName);
        if (data) {
            try {
                return fileData = JSON.parse(data);
            } catch (e) {
                console.log('The data in the file isn\'t a valid json object');
                return 'Not valid json';
            }
        }
        console.error('An error occurred while reading the file');
        return 'Error reading the file';
    };

    async getValue(key) {
        await this.readFile();
        if (fileData) {
            console.log(`Check if value exists for '${key}' ...`);
            if (!Object.keys(fileData).includes(key)) {
                console.log(`No value exists for '${key}' ...`);
                return false;
            } else {
                return fileData[key].toString();
            }
        }
    };

    async add(key, value) {
        if (!key || !value) {
            const result = 'Key and value must provided';
            console.log(result);
            return result;
        }
        const keyValue = await this.getValue(key);
        if (fileData) {
            if (keyValue) {
                console.log(`This key '${key}' is existed, can\'t insert duplicated keys`);
                return 'Duplicated key';
            } else {
                fileData[key] = value;
                await fs.writeFileSync(fileName, JSON.stringify(fileData));
                console.log(`Value '${value}' inserted for key '${key}'`);
                return 'Successfully inserted';
            }
        }
    }

    async list() {
        await this.readFile();
        if (fileData) {
            const keys = Object.keys(fileData);
            if (keys.length) {
                console.log('Existing key/value:');
                keys.forEach(key => {
                    console.log(` - ${key}: ${fileData[key]}`)
                });
                return 'List of data';
            } else {
                const error = 'No data found';
                console.log(error);
                return error;
            }
        }
    }

    async get(key) {
        if (!key) {
            const error = 'Key must be provided';
            console.log(error);
            return error;
        }
        const keyValue = await this.getValue(key);
        if (fileData) {
            if (keyValue) {
                console.log(`The value for key '${key}' is: '${keyValue}'`);
                return keyValue;
            } else {
                console.log(`There is no such value inserted for key '${key}'`);
                return 'No value inserted for that key';
            }
        }
    }


    async remove(key) {
        if (!key) {
            const error = 'Key must be provided';
            console.log(error);
            return error;
        }
        const keyValue = await this.getValue(key);
        if (fileData) {
            if (keyValue) {
                delete fileData[key];
                await fs.writeFileSync(fileName, JSON.stringify(fileData));
                console.log(`Key '${key}' removed successfully`);
                return 'Successfully removed';
            } else {
                console.log(`There is no such value inserted for key '${key}'`);
                return 'No such key';
            }
        }
    }

    async clear() {
        await fs.writeFileSync(fileName, '{}');
        console.log('File cleared successfully');
        return 'Successfully cleared';
    }
}

module.exports = Dictionary;