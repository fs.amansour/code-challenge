#! /usr/bin/env node

const Dictionary = require('./dictionary');

const command = process.argv[2];
const key = process.argv[3];
const value = process.argv[4];

const dictionary = new Dictionary();

const notvalidCommand = () => {
    console.log(`The command you entered isn't a valid command you can use,
        add key value
        list
        get key
        remove key
        clear`);

};

if (!command) {
    notvalidCommand();
} else {
    switch (command.toLowerCase()) {
        case 'add':
            dictionary.add(key, value);
            break;

        case 'list':
            dictionary.list();
            break;

        case 'get':
            dictionary.get(key);
            break;

        case 'remove':
            dictionary.remove(key);
            break;

        case 'clear':
            dictionary.clear();
            break;

        default:
            notvalidCommand();
            break;
    }
}

