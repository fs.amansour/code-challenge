const fs = require('fs');
const fileName = 'data.json';

const Dictionary = require('./dictionary');
const dictionary = new Dictionary();

describe('Test store', () => {
    beforeEach(() => {
        dictionary.clear();
    });

    it('should return error if key or value not provided', async () => {
        const res = await dictionary.add();
        expect(res).toBe('Key and value must provided');
    });

    it('should return error if try to insert duplicated key', async () => {
        await dictionary.add('key', 'value');
        const res = await dictionary.add('key', 'value');
        expect(res).toBe('Duplicated key');
    });

    it('should return success if key inserted successfully', async () => {
        const res = await dictionary.add('key', 'value');
        expect(res).toBe('Successfully inserted');
    });

    it('should return No data found if there are no data in the file', async () => {
        const res = await dictionary.list();
        expect(res).toBe('No data found');
    });

    it('should return a list of data', async () => {
        await dictionary.add('key', 'value');
        const res = await dictionary.list();
        expect(res).toBe('List of data');
    });

    it('should return error if key not provided', async () => {
        const res = await dictionary.get();
        expect(res).toBe('Key must be provided');
    });

    it('should return error if key not found', async () => {
        const res = await dictionary.get('key');
        expect(res).toBe('No value inserted for that key');
    });

    it('should return the value of the inserted key', async () => {
        await dictionary.add('key', 'value');
        const res = await dictionary.get('key');
        expect(res).toBe('value');
    });

    it('should return error if key not provided', async () => {
        const res = await dictionary.remove();
        expect(res).toBe('Key must be provided');
    });

    it('should return success if key removed successfully', async () => {
        await dictionary.add('key', 'value');
        const res = await dictionary.remove('key');
        expect(res).toBe('Successfully removed');
    });

    it('should return success if key removed successfully', async () => {
        const res = await dictionary.remove('key');
        expect(res).toBe('No such key');
    });

    it('should return success if file cleared successfully', async () => {
        const res = await dictionary.clear();
        expect(res).toBe('Successfully cleared');
    });

    it('should create the file if it isn\'t existed', async () => {
        await fs.unlinkSync(fileName);
        const res = await dictionary.createFileIfNotExisted();
        expect(res).toBe('File created');
    });

    it('should return error if json data isn\'t valid', async () => {
        await fs.writeFileSync(fileName, '');
        const res = await dictionary.readFile();
        expect(res).toBe('Not valid json');
    });
});